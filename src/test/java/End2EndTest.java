import it.eng.ten.identities.lib.config.Config;
import it.eng.ten.identities.lib.model.crypto.ECDSApublicKeyXY;
import it.eng.ten.identities.lib.model.crypto.FileWallet;
import it.eng.ten.identities.lib.model.crypto.Signature;
import it.eng.ten.identities.lib.model.identity.Identity;
import it.eng.ten.identities.lib.model.identity.IdentityBase;
import it.eng.ten.identities.lib.model.identity.IdentityStatus;
import it.eng.ten.identities.lib.utils.ECDSA;
import it.eng.ten.identities.lib.utils.JsonHandler;
import org.hyperledger.fabric.gateway.GatewayException;
import org.junit.Test;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static it.eng.ten.identities.lib.utils.MethodUtils.searchIdentity;


public class End2EndTest {


    //@Test
    public void createEndorserTest() {
        try {
            FileWallet fileWallet = searchIdentity("C1H6ZmEsVorKZMTdqUV8NxgZSkk87zpRUBKRqKmDbQEz", "eng", System.getProperty("user.home"));
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decoder.decode(fileWallet.getkeyPair().getPublicKey())));
            PrivateKey decodedPrivateKey =
                    keyFactory.generatePrivate(new PKCS8EncodedKeySpec(decoder.decode(fileWallet.getkeyPair().getPrivateKey())));


            Identity identity = new Identity();
            IdentityStatus identityStatus = new IdentityStatus();
            identity.setIdentityStatus(identityStatus);
            IdentityBase identityBase = new IdentityBase();
            ECDSApublicKeyXY ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(publicKey);
            identityBase.setpKey(JsonHandler.convertToJson(ecdsApublicKeyXY));
            identityBase.setEnd("");
            identityBase.setExt("ROOT");
            identityBase.setkType("ECDSA");
            identity.setIdentityBase(identityBase);

            Config config = new Config("Org1MSP", "mychannel", "admin-ten-identity", "tenIdentitiesInterQ", "connection-org1.json", "C:\\Users\\cbarberini\\wallet", "C:\\Users\\cbarberini\\wallet");

            it.eng.ten.identities.lib.TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
            it.eng.ten.identities.lib.model.Response response = identitiesService.submitTransaction("C1H6ZmEsVorKZMTdqUV8NxgZSkk87zpRUBKRqKmDbQEz", "eng", "postIdentity", JsonHandler.convertToJson(identity));

            if (response.getHeader().getCode() == 201) {
                assert true;
            } else assert false;
        } catch (Exception e) {

            assert false;
        }
    }

    //@Test
    public void testGetIdentity() throws GatewayException {
        String cid = "51qQzAtfP6HMBK9zrhNykGg2PSQ9EeFucxHY43uc8SX8CIAO";
        String id = "51qQzAtfP6HMBK9zrhNykGg2PSQ9EeFucxHY43uc8SX8";
        String walletPath = "C:\\Users\\cbarberini\\wallet";
        String functionName = "getIdentity";
        Config config = new Config("Org1MSP", "mychannel",
                "admin-tenid", "tenid-chaincode", "connection-org1.json", walletPath, walletPath);

        it.eng.ten.identities.lib.TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
        try {
            it.eng.ten.identities.lib.model.Response response = identitiesService.submitTransaction(cid, "engineering",
                    functionName, cid + "@" + id);
            if (response.getMessage().isEmpty()) {
                assert false;
            } else {
                System.out.println("Response arrived with data: " + response.toString());
                assert true;
            }
        } catch (GatewayException e) {
            System.err.println(e.getMessage());
            assert false;
        }
    }

    //@Test
    public void TestEditIdentity() throws Exception {
        String id = "CowT7kXtSsKYbtL2ccifP2EPsAqxg6xo5FP9C5MFHPGJ";
        String controllerAddress = "C1H6ZmEsVorKZMTdqUV8NxgZSkk87zpRUBKRqKmDbQEz";
        String ext = "TEST TEST TEST test";
        Identity identity = new Identity();
        IdentityStatus identityStatus = new IdentityStatus();
        IdentityBase identityBase = new IdentityBase();
        identityBase.setId(id);
        identityBase.setEnd(controllerAddress);
        identityBase.setExt(ext);
        identity.setIdentityBase(identityBase);
        identity.setIdentityStatus(identityStatus);

        Config config = new Config("Org1MSP", "mychannel", "admin-ten-identity", "tenIdentitiesInterQ", "connection-org1.json", "C:\\Users\\cbarberini\\wallet", "C:\\Users\\cbarberini\\wallet");

        it.eng.ten.identities.lib.TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
        it.eng.ten.identities.lib.model.Response response1 = identitiesService.submitTransaction(controllerAddress, "eng", "editIdentity", JsonHandler.convertToJson(identity));

        System.out.println(response1.getHeader().getCode());
//        switch (functionName) {
//            case "revoke":
//                return identitiesService.submitTransaction(controllerAddress, password, "revokeIdentity", JsonHandler.convertToJson(identity));
//            case "activate":
//                return identitiesService.submitTransaction(controllerAddress, password, "activateIdentity", JsonHandler.convertToJson(identity));
//            case "suspend":
//                return identitiesService.submitTransaction(controllerAddress, password, "suspendIdentity", JsonHandler.convertToJson(identity));
//        }
    }

    //@Test
    public void testPostIdentity() {
        try {
            String end = "end";
            String ext = "";
            String pubKey = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEnSnb6uWxq28Exy1MGm873/GcnRyNlTBW51D3q5HhN9pHmhQZdlvEZeHQsHUQv8zFgnTV5fkXbtA1Ur+7rDSGtA==";


            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            Base64.Decoder decoder = Base64.getDecoder();

            Identity identity = new Identity();
            IdentityStatus identityStatus = new IdentityStatus();
            identity.setIdentityStatus(identityStatus);
            IdentityBase identityBase = new IdentityBase();
            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decoder.decode(pubKey)));
            ECDSApublicKeyXY ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(publicKey);
            identityBase.setpKey(JsonHandler.convertToJson(ecdsApublicKeyXY));
            identityBase.setEnd(end);
            identityBase.setExt(ext);
            identityBase.setkType("ECDSA");
            identity.setIdentityBase(identityBase);

            Config config = new Config("Org1MSP", "mychannel", "admin-ten-identity", "testetstetsts", "connection-org1.json", "C:\\Users\\cbarberini\\wallet", "C:\\Users\\cbarberini\\wallet");
            it.eng.ten.identities.lib.TenIdentitiesService identitiesService = new it.eng.ten.identities.lib.TenIdentitiesServiceImpl(config);
            it.eng.ten.identities.lib.model.Response response = identitiesService.submitTransaction("C1H6ZmEsVorKZMTdqUV8NxgZSkk87zpRUBKRqKmDbQEz", "eng", "postIdentity", JsonHandler.convertToJson(identity));

        } catch (GatewayException e) {

            System.out.println("FINAL PRINT ERROR" + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

