package it.eng.ten.identities.lib;

import it.eng.ten.identities.lib.model.Response;
import org.hyperledger.fabric.gateway.GatewayException;

/**
 * @author clod16
 * @project ten-identities-lib
 * @date 28/10/2021
 */
public interface TenIdentitiesService {


    /**
     * @param addr,      identity address, mandatory if the method requires digital signature, empty string otherwise
     * @param walletPwd, password to decrypt the wallet if the method requires digital signature, empty string otherwise
     * @param funcName,  name of chaincode method to invoke
     * @param funcArgs,  json-object of args that pass to functionName of chaincode
     * @throws GatewayException
     */
    Response submitTransaction(String addr, String walletPwd, String funcName, String funcArgs) throws GatewayException;


}
