package it.eng.ten.identities.lib;

import it.eng.ten.identities.lib.config.Config;
import it.eng.ten.identities.lib.model.Response;
import it.eng.ten.identities.lib.model.crypto.FileWallet;
import it.eng.ten.identities.lib.model.crypto.Signature;
import it.eng.ten.identities.lib.utils.ChiperUtils;
import it.eng.ten.identities.lib.utils.ECDSA;
import it.eng.ten.identities.lib.utils.JsonHandler;
import it.eng.ten.identities.lib.utils.MethodUtils;
import org.apache.commons.lang3.StringUtils;
import org.hyperledger.fabric.gateway.*;
import org.hyperledger.fabric.sdk.ProposalResponse;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * @author clod16
 * @project ten-identities-lib
 * @date 28/10/2021
 */
public class TenIdentitiesServiceImpl implements TenIdentitiesService {


    public static final String SEPARATOR_GET_ARGS = "@";
    private final String GET_FUNCTION_NAME = "getIdentity";
    private static final int TIMEOUT_VALUE = 5;
    private static final int INITIAL_DELAY = 700;
    private static final String TEN_ID_CHALLENGE_RESPONSE = "TenIdChallengeResponse";

    private static final Logger log = Logger.getLogger(TenIdentitiesServiceImpl.class.getName());
    private Gateway.Builder gatewayBuilder;
    private final Config config;

    public TenIdentitiesServiceImpl(Config config) throws GatewayException {
        this.config = config;
        connect();
    }


    private void connect() throws GatewayException {
        try {
            gatewayBuilder = Gateway.createBuilder();
            gatewayBuilder.discovery(false);
            createWalletFromFileSystem();
            gatewayBuilder.identity(createWalletFromFileSystem(), this.config.getUserName()).networkConfig(Paths.get(this.config.getWalletFabricPath() + File.separator + this.config.getNetworkFilename()));
        } catch (Exception e) {
            log.severe("Error encountered in connecting to Blockchain with trace... " + e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Error encountered in connecting to " +
                    "Blockchain", 502);
            throw new GatewayException(JsonHandler.convertToJson(response));

        }
    }

    private Wallet createWalletFromFileSystem() throws GatewayException {
        Wallet wallet;
        try {
            final Path path = Paths.get(this.config.getWalletFabricPath());
            wallet = Wallets.newFileSystemWallet(path);
        } catch (IOException e) {
            log.severe("Error encountered in connecting creating Wallet from fileSystem with trace... " + e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Error encountered in connecting creating " +
                    "Wallet", 500);
            throw new GatewayException(JsonHandler.convertToJson(response));
        }
        return wallet;
    }


    private Contract getContract() throws GatewayException {
        try {
            Gateway gateway = gatewayBuilder.connect();
            Network network = gateway.getNetwork(this.config.getChannelName());
            log.info("Connect correctly to HLF network channel" + this.config.getChannelName() + " found correctly");
            Contract contract = network.getContract(this.config.getChaincodeName());
            log.info("Chaincode" + this.config.getChaincodeName() + " found correctly");
            return contract;

        } catch (Exception e) {
            log.severe("Error encountered in retrieving chaincode with trace... " + e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Error encountered in retrieving chaincode",
                    502);
            throw new GatewayException(JsonHandler.convertToJson(response));
        }
    }


    private Future<Object> executeCallable(Contract contract, boolean useDelay, String functionName,
                                           String... chaincodeArgs) throws GatewayException {
        final String operation = functionName;
        Callable<Object> task = () -> {
            String[] args = Arrays.copyOfRange(chaincodeArgs, 0, chaincodeArgs.length);
            try {
                final byte[] submit = contract.submitTransaction(operation, args);
                return (Response) JsonHandler.convertFromJson(new String(submit, StandardCharsets.UTF_8),
                        Response.class);

            } catch (Exception e) {
                buildResponseStatusException(e);
            }
            return null;
        };
        ScheduledExecutorService scheduledExecutor = null;
        ExecutorService executor = null;
        try {
            if (useDelay) {
                scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
                return scheduledExecutor.schedule(task, INITIAL_DELAY, TimeUnit.MILLISECONDS);
            } else {
                executor = Executors.newSingleThreadScheduledExecutor();
                return executor.submit(task);
            }
        } catch (Exception e) {
            log.severe("Error encountered in computing chaincode function with trace... " + e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Error encountered in computing chaincode " +
                    "function", 502);
            throw new GatewayException(JsonHandler.convertToJson(response));
        } finally {
            if (scheduledExecutor != null) scheduledExecutor.shutdown();
            if (executor != null) executor.shutdown();
        }
    }


    @Override
    public Response submitTransaction(String identityAddress, String psw, String functionName, String chaincodeArgs) throws GatewayException {
        log.info("Configuration lib : " + this.config.toString());
        Contract contract = getContract();
        Future<Object> objectFuture = null;
        log.info("Signature generation started...");
        Base64.Decoder decoder = Base64.getDecoder();
        try {
            KeyFactory keyFactory = null;
            try {
                keyFactory = KeyFactory.getInstance("EC");
            } catch (NoSuchAlgorithmException e) {
                Response response = MethodUtils.createResponseException(e, e.getMessage(), 500);
                throw new GatewayException(JsonHandler.convertToJson(response));
            }
            FileWallet fileWallet;
            if (StringUtils.isEmpty(identityAddress)) {  //POST ENDORSER
                it.eng.ten.identities.lib.model.identity.Identity identity =
                        (it.eng.ten.identities.lib.model.identity.Identity) JsonHandler.convertFromJson(chaincodeArgs
                                , it.eng.ten.identities.lib.model.identity.Identity.class);
                final String addr = ChiperUtils.calculateAddr(identity.getIdentityBase().getpKey());
                fileWallet = MethodUtils.searchIdentity(addr, psw, this.config.getWalletIdentitiesPath());
            } else fileWallet = MethodUtils.searchIdentity(identityAddress, psw, this.config.getWalletIdentitiesPath());
            PrivateKey decodedPrivateKey = null;
            try {
                decodedPrivateKey =
                        keyFactory.generatePrivate(new PKCS8EncodedKeySpec(decoder.decode(fileWallet.getkeyPair().getPrivateKey())));
            } catch (InvalidKeySpecException e) {
                Response response = MethodUtils.createResponseException(e, e.getMessage(), 500);
                throw new GatewayException(JsonHandler.convertToJson(response));

            }
            String challenge;
            final boolean isGET = GET_FUNCTION_NAME.equals(functionName) && chaincodeArgs.contains(SEPARATOR_GET_ARGS);
            if (isGET) {
                String[] args = chaincodeArgs.split(SEPARATOR_GET_ARGS);
                challenge =
                        functionName + args[0] + args[1] + config.getOrganizationMspId().toLowerCase() + config.getUserName().toLowerCase();
            } else
                challenge =
                        functionName + chaincodeArgs + config.getOrganizationMspId().toLowerCase() + config.getUserName().toLowerCase();
            Signature signature = null;
            try {
                signature = ECDSA.signMsg(challenge, decodedPrivateKey);
            } catch (Exception e) {
                Response response = MethodUtils.createResponseException(e, "Error in signature message", 500);
                throw new GatewayException(JsonHandler.convertToJson(response));
            }
            log.info("Signature generation finished. Call chaincode with authentication mechanism.");
            if (isGET) {
                String[] args = chaincodeArgs.split(SEPARATOR_GET_ARGS);
                objectFuture = executeCallable(contract, true, functionName, args[0], args[1],
                        JsonHandler.convertToJson(signature));
            } else
                objectFuture = executeCallable(contract, true, functionName, chaincodeArgs,
                        JsonHandler.convertToJson(signature));
            Response response = (Response) objectFuture.get(TIMEOUT_VALUE, TimeUnit.SECONDS);
            log.info("submitTransaction completed with Response code: " + response.getHeader().getCode());
            return response;
        } catch (NoSuchAlgorithmException | InterruptedException | ExecutionException | TimeoutException e) {
            if (!MethodUtils.isResponseJson(e.getMessage())) {
                Response response = MethodUtils.createResponseException(e, e.getMessage(), 500);
                throw new GatewayException(JsonHandler.convertToJson(response));
            } else {
                throw new GatewayException(MethodUtils.removeExceptionClassFromMsg(e));
            }
        }
    }


    private void buildResponseStatusException(Exception e) throws Exception {
        if (e instanceof ContractException) {
            final Collection<ProposalResponse> proposalResponses = ((ContractException) e).getProposalResponses();
            for (ProposalResponse response : proposalResponses) {
                String message = response.getProposalResponse().getResponse().getMessage();
                if (MethodUtils.isResponseJson(message)) {
                    Response response1 =
                            (Response) JsonHandler.convertFromJson(response.getProposalResponse().getResponse().getMessage(), Response.class);
                    throw new GatewayException(JsonHandler.convertToJson(response1));
                } else {
                    throw new GatewayException(message);
                }
            }
        } else if (e instanceof TimeoutException) {
            throw new GatewayException(MethodUtils.removeExceptionClassFromMsg(e));
        }
    }


}