package it.eng.ten.identities.lib.model.identity;

public class Ext {
    private String extType;
    private String payload;

    public Ext(String extType, String payload) {
        this.extType = extType;
        this.payload = payload;
    }

    public Ext() {
    }

    public String getExtType() {
        return extType;
    }

    public void setExtType(String extType) {
        this.extType = extType;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Ext{" +
                "extType='" + extType + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }
}
