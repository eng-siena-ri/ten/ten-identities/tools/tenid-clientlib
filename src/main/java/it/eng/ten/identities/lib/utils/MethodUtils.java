package it.eng.ten.identities.lib.utils;


import it.eng.ten.identities.lib.model.Header;
import it.eng.ten.identities.lib.model.Response;
import it.eng.ten.identities.lib.model.crypto.FileWallet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.hyperledger.fabric.gateway.GatewayException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;


public class MethodUtils {

    private final SimpleDateFormat rfc3339 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static final Logger log = Logger.getLogger(MethodUtils.class.getName());
    public static final String WALLET_TEN_NAME = "identities-wallet";


    public static boolean checkString(String str) {
        return str != null && !str.trim().isEmpty();
    }


    public static int getKeyLength(final PublicKey pk) {
        int len = -1;
        final ECPublicKey ecpriv = (ECPublicKey) pk;
        final java.security.spec.ECParameterSpec spec = ecpriv.getParams();
        if (spec != null) {
            len = spec.getOrder().bitLength(); // does this really return something we expect?
        } else {
            // We support the key, but we don't know the key length
            len = 0;
        }
        return len;
    }


    public static Response createResponseException(Exception e, String msg, int code) {
        String message = removeExceptionClassFromMsg(e);
        return new Response(new Header(code, message), msg);
    }

    public static String removeExceptionClassFromMsg(Exception e) {
        String message = e.getMessage();
        String className = e.getClass().getCanonicalName();
        if (message.contains(className)) {
            message = StringUtils.replace(message, className + ":", "");
        }
        return message;
    }

    public static FileWallet searchIdentity(String controllerAddress, String psw, String tenWalletPath) throws GatewayException {
        String fileToString = null;
        try {
            fileToString = FileUtils.readFileToString(new File(tenWalletPath + File.separator + WALLET_TEN_NAME),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.severe(e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Wallet not found!", 500);
            throw new GatewayException(JsonHandler.convertToJson(response));
        }

        String dec;
        try {
            dec = ChiperUtils.decrypt(psw, fileToString);
        } catch (GatewayException e) {
            log.severe(e.getMessage());
            Response response = MethodUtils.createResponseException(e, "Password wrong, try whit different password",
                    500);
            throw new GatewayException(JsonHandler.convertToJson(response));
        }
        Collection<FileWallet> fileWallets = null;
        try {
            fileWallets = (Collection<FileWallet>) JsonHandler.convertFromJson(dec, FileWallet.class, true);
        } catch (GatewayException e) {
            Response response = MethodUtils.createResponseException(e, "Internal error", 500);
            throw new GatewayException(JsonHandler.convertToJson(response));
        }

        for (FileWallet fileWallet : fileWallets
        ) {
            if (fileWallet.getAddress().equals(controllerAddress)) {
                log.info("Controller identity extract correctly ");
                return fileWallet;
            }
        }
        Response response = MethodUtils.createResponseException(new GatewayException("Identity not found in wallet"), "Identity not found in wallet with address:" + controllerAddress, 500);
        throw new GatewayException(JsonHandler.convertToJson(response));

    }

    public static String createGUID() {
        Random random = ThreadLocalRandom.current();
        byte[] r = new byte[64];
        random.nextBytes(r);
        return Base64.getEncoder().encodeToString(r).replace("/", "");
    }


    private static String generateRandomString() {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(10);

        for (int i = 0; i < 10; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public static String checkExceptionCause(Exception e) {
        if (null != e.getCause()) {
            return e.getCause().getMessage();
        } else {
            return e.getMessage();
        }
    }

    public static boolean isResponseJson(String s) {

        return s.contains("header") && s.contains("description") && s.contains("message");
    }

    private String toRFC3339(Date d) {
        return rfc3339.format(d);
    }


}
