package it.eng.ten.identities.lib.model.crypto;


public class FileWallet {

    private String address;
    private KeyPair keyPair;
    private Status status;

    public FileWallet() {
    }

    public FileWallet(String address, KeyPair keyPair) {
        this.address = address;
        this.keyPair = keyPair;
    }

    public FileWallet(String address, KeyPair keyPair, Status status) {
        this.address = address;
        this.keyPair = keyPair;
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public KeyPair getkeyPair() {
        return keyPair;
    }

    public void setkeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FileWallet{" +
                "address='" + address + '\'' +
                ", keyPair=" + keyPair +
                '}';
    }
}