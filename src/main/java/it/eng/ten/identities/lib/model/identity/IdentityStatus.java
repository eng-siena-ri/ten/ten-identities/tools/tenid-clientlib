package it.eng.ten.identities.lib.model.identity;

public class IdentityStatus {

    private String id;
    private String start;
    private Integer status;

    public IdentityStatus(String id, String start, Integer status) {
        this.id = id;
        this.start = start;
        this.status = status;
    }

    public IdentityStatus() {
        this.status = IdentityStatusValue.ACTIVE_STATUS.ordinal();  //Active
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "IdentityStatus{" +
                "id='" + id + '\'' +
                ", start='" + start + '\'' +
                ", status=" + status +
                '}';
    }
}
