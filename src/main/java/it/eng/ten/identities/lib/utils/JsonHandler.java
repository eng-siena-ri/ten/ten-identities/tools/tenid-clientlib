package it.eng.ten.identities.lib.utils;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.owlike.genson.Genson;
import org.apache.commons.lang3.StringUtils;
import org.hyperledger.fabric.gateway.GatewayException;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class JsonHandler {
    private static final Logger log = Logger.getLogger(JsonHandler.class.getName());
    public static final String UTF_8 = "UTF-8";

    public static String convertToJson(Object obj) throws GatewayException {
        return convertToJsonByJackson(obj);
    }

    public static Object convertFromJson(String json, Class clazz) throws GatewayException {
        return convertFromJsonByJackson(json, clazz);
    }



    public static String convertToJsonByJackson(Object obj) throws GatewayException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            //mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE); //This property put data in upper camel case
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new GatewayException(e.getMessage());
        }
    }

    public static String convertToJsonByGenson(Object obj) throws GatewayException {
        try {
            final Genson genson = new Genson();
            return genson.serialize(obj);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new GatewayException(e.getMessage());
        }
    }


    public static Object convertFromJsonByJackson(String json, Class clazz) throws GatewayException {
        try {
            if (StringUtils.isEmpty(json))
                throw new Exception("Json data is EMPTY");
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true); //This property serialize/deserialize not considering the case of fields
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new GatewayException(e.getMessage());
        }
    }

    public static Object convertFromJsonByGenson(String json, Class clazz) throws GatewayException {
        try {
            if (StringUtils.isEmpty(json))
                throw new Exception("Json data is EMPTY");
            Genson genson = new Genson();
            return genson.deserialize(json, clazz);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new GatewayException(e.getMessage());
        }
    }

    public static Object convertFromJson(String json, Class clazz, boolean isCollection) throws GatewayException {
        try {
            if (StringUtils.isEmpty(json))
                throw new Exception("Json data is EMPTY");
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true); //This property serialize/deserialize not considering the case of fields
            if (isCollection) {
                //ObjectReader objectReader = mapper.reader().forType(new TypeReference<List<?>>() {
                //});
                // return objectReader.readValue(json);
                return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.severe(e.getMessage());
            throw new GatewayException(e);
        }
    }

    public static void editPathConnection(String path) {

    }

}
