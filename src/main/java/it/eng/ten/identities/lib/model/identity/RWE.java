package it.eng.ten.identities.lib.model.identity;

public class RWE {

    private String nature;
    private String payload;

    public RWE() {
    }

    public RWE(String nature, String payload) {
        this.nature = nature;
        this.payload = payload;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "RWE{" +
                "nature='" + nature + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }
}

