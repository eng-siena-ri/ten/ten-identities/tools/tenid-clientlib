package it.eng.ten.identities.lib.model.identity;

public class IdentityBase {

    private String id;
    private String pKey;
    private String kType;
    private String end;
    private String ext;

    public IdentityBase(String id, String pKey, String kType, String end, String ext) {
        this.id = id;
        this.pKey = pKey;
        this.kType = kType;
        this.end = end;
        this.ext = ext;
    }

    public IdentityBase() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpKey() {
        return pKey;
    }

    public void setpKey(String pKey) {
        this.pKey = pKey;
    }

    public String getkType() {
        return kType;
    }

    public void setkType(String kType) {
        this.kType = kType;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "IdentityBase{" +
                "id='" + id + '\'' +
                ", pKey='" + pKey + '\'' +
                ", kType='" + kType + '\'' +
                ", end='" + end + '\'' +
                ", ext='" + ext + '\'' +
                '}';
    }
}
