package it.eng.ten.identities.lib.model.identity;

public class PIdS {

    private String role;
    private String payload;

    public PIdS() {
    }

    public PIdS(String role, String payload) {
        this.role = role;
        this.payload = payload;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "PIds{" +
                "role='" + role + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }
}
