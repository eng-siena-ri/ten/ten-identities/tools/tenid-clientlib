package it.eng.ten.identities.lib.model.crypto;

public class KeyPair {

    private String publicKey;
    private String privateKey;
    private String keyType;
    private String name;

    public KeyPair() {
    }

    public KeyPair(String publicKey, String privateKey, String keyType, String name) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.keyType = keyType;
        this.name = name;
    }

    public KeyPair(String publicKey, String privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "KeyPair{" +
                "publicKey='" + publicKey + '\'' +
                ", privateKey='" + privateKey + '\'' +
                ", keyType='" + keyType + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}