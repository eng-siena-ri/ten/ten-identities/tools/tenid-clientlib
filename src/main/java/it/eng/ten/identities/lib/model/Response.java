package it.eng.ten.identities.lib.model;

public class Response {

    private Header header;
    private String message;

    public Response() {
    }

    public Response(Header header, String message) {
        this.header = header;
        this.message = message;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
