package it.eng.ten.identities.lib.model;

/**
 * @author ascatox
 * @since 04/11/21
 */
public class Header {
    public Header() {
    }

    public Header(int code, String description) {
        this.code = code;
        this.description = description;
    }

    private int code;
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
