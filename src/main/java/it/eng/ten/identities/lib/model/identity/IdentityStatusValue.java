package it.eng.ten.identities.lib.model.identity;

/**
 * @author Antonio Scatoloni on 14/10/2020
 **/

public enum IdentityStatusValue {
    EMPTY_STATUS, ACTIVE_STATUS, SUSPEND_STATUS, REVOKE_STATUS
}
