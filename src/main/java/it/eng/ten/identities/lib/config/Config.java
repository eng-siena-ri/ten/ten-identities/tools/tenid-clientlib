package it.eng.ten.identities.lib.config;

/**
 * @author clod16
 * @project ten-identities-lib
 * @date 28/10/2021
 */
public class Config {


    private final String organizationMspId;
    private final String channelName;
    private final String userName;
    private final String chaincodeName;
    private final String networkFilename;
    private final String walletIdentitiesPath;
    private final String walletFabricPath;

    public Config(String organizationMspId, String channelName, String userName, String chaincodeName, String networkFilename, String walletIdentitiesPath, String walletFabricPath) {
        this.organizationMspId = organizationMspId;
        this.channelName = channelName;
        this.userName = userName;
        this.chaincodeName = chaincodeName;
        this.networkFilename = networkFilename;
        this.walletIdentitiesPath = walletIdentitiesPath;
        this.walletFabricPath = walletFabricPath;
    }


    public String getOrganizationMspId() {
        return organizationMspId;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getUserName() {
        return userName;
    }

    public String getChaincodeName() {
        return chaincodeName;
    }

    public String getNetworkFilename() {
        return networkFilename;
    }


    public String getWalletIdentitiesPath() {
         return walletIdentitiesPath;
    }

    public String getWalletFabricPath() {
        return walletFabricPath;
    }

    @Override
    public String toString() {
        return "Config{" +
                "organizationMspId='" + organizationMspId + '\'' +
                ", channelName='" + channelName + '\'' +
                ", userName='" + userName + '\'' +
                ", chaincodeName='" + chaincodeName + '\'' +
                ", networkFilename='" + networkFilename + '\'' +
                ", walletIdentitiesPath='" + walletIdentitiesPath + '\'' +
                ", walletFabricPath='" + walletFabricPath + '\'' +
                '}';
    }
}
