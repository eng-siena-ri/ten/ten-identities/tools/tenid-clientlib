package it.eng.ten.identities.lib.model.identity;

public class Identity {
    private IdentityBase identityBase;
    private IdentityStatus identityStatus;


    public Identity(IdentityBase identityBase, IdentityStatus identityStatus) {
        this.identityBase = identityBase;
        this.identityStatus = identityStatus;
    }

    public Identity() {
    }

    public IdentityBase getIdentityBase() {
        return identityBase;
    }

    public void setIdentityBase(IdentityBase identityBase) {
        this.identityBase = identityBase;
    }

    public IdentityStatus getIdentityStatus() {
        return identityStatus;
    }

    public void setIdentityStatus(IdentityStatus identityStatus) {
        this.identityStatus = identityStatus;
    }

    @Override
    public String toString() {
        return "Identity{" +
                "identityBase=" + identityBase +
                ", identityStatus=" + identityStatus +
                '}';
    }
}
