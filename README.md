# tenid-clientlib

**tenid-clientlib**  is a java library which allows a client to interface and call a chaincode.
The library allows you to use the authentication mechanism by creating a digital signature if the chaincode needs it.
This library exposes an interface with a method called **submitTransaction**.
```
public interface TenIdentitiesService {  
    /**
     * @param addr,      identity address, mandatory if the method requires digital signature, empty string otherwise
     * @param walletPwd, password to decrypt the wallet if the method requires digital signature, empty string otherwise
     * @param funcName,  name of chaincode method to invoke
     * @param funcArgs,  json-object of args that pass to functionName of chaincode
     * @throws GatewayException
     */
    Response submitTransaction(String addr, String walletPwd, String funcName, String funcArgs) throws GatewayException;

}
```
## Configuration

To instantiate the library you must first build an object called Config by setting some configurations through the class constructor, all of which are mandatory
```
public class Config {  
	 private final String organizationMspId;  
	 private final String channelName;  
	 private final String userName;  
	 private final String chaincodeName;  
	 private final String networkFilename;  
	 private final String walletIdentitiesPath;  
	 private final String walletFabricPath;
```
- **organizationMspId** : Name of Fabric organizationMSPId.
-  **channelName** : name of the channel where the chiancode is installed.
- **userName** : Identity name of the fabric certificate used to authenticate to the blockchain (es. admin).
- **chaincodeName** : Name of chaincode.
- **networkFilename** : name of the fabric network configuration file.
- **walletIdentitiesPath** : directory path of the identity wallet generated with **tenid-cli** without file name.
- **walletFabricPath** : path of the fabric wallet where there are the certificates and the network file.

## Usage
The library will always respond with an object of type Response
```
 public class Response {  
  
 private Header header;  
 private String message;
```
In the **message** string there will be the value of the chaincode response in case of valid operation
```
 public class Header {  
    private int code;  
		private String description;
```
In the Header object we find two fields, an entire code that identifies the status code of the operation performed (as for the http protocol, es. 201, 404....) and a description field where it will be possible to find a small description of the code just received.
## Example
```
Config config = new Config(.....);
TenIdentitiesService identitiesService = new TenIdentitiesServiceImplconfig);
Response response = identitiesService.submitTransaction(<endorser address>, <wallet password>, <chaincodeFunctionName> , <args> 
if(response.getHeader.getCode() == 200){
    responde.getMessage();
  }
```

## Exceptions

The library uses its own exception class called TenIdentitiesException which extends the Java Exception class.
All exceptions are provided with a readable and easy to understand message.

## License <a name="license"></a>

tenid-clientlib Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.


